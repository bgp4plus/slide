![KSA](https://gitlab.com/wagtail9019/slide/blob/master/img/SST.svg)
## 九州学生エンジニア勉強会 #?
### at hogehoge

---

# about us

---

## Kyushu Student Associationとは？
- 九州地方に住む学生エンジニアを支援するために誕生した団体です
- 福岡を中心に九州各地で勉強会やLT大会を開催し，学生同士の交流を推進
- オンラインではDiscord，オフラインでは勉強会などで交流しています
- 詳しくは https://kyushu.gr.jp をチェック！

---

# Sponsors

---

## Gold Sponsors
![SST](https://gitlab.com/wagtail9019/slide/blob/master/img/SST.svg)

---

## Other Sponsors
シルバースポンサー![pepabo](https://gitlab.com/wagtail9019/slide/blob/master/img/pepabo.svg)
ブロンズスポンサー![supporterz](https://gitlab.com/wagtail9019/slide/blob/master/img/supporterz.svg)
アンバサダー
![cybozu](https://gitlab.com/wagtail9019/slide/master/img/cybozu.svg)
![techbowl](https://gitlab.com/wagtail9019/slide/blob/master/img/techbowl.svg)

---

## 会場
- hogehoge様にご提供いただきました！
- facebookでチェックインしよう！

---

# Information

---

## アジェンダ
- hoge1
- hoge2

---

## ハッシュタグ
### student_kyushuでツイート

---

## 写真撮影について
- 広報のために本勉強会の様子を撮影し，SNSに投稿する場合があります
- 露出を避けたい方は運営までお知らせください
   (事前にconnpassのアンケートでお答えいただいている場合は結構です)

---

